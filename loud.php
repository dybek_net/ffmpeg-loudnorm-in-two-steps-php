<?php
/**
 * loud.php
 * Daniel Dybek 04.11.2021
 * 
 * requires ffmpeg and some bash basics you can see below.
 * 
 * type filename as parameter after script name, eg:
 * ./path/to/this/loud.php filename.mp3
 * 
 * the file will be overwritten.
 */
$target_I=      -16;
$target_TP=     -1.5;
$target_LRA=    11;

if(count($argv) == 1) {
    echo "One parameter required!\n";
    exit;
} else {
    $fileName = $argv[1];
    $tempFileName = str_replace('.mp3', '_tempNormalized.mp3', $fileName);
    echo $fileName;
}
if (!preg_match("/^.+(\.mp3)$/", $fileName)){
    echo "Try again with valid filename\n";
    exit;
}
if (!file_exists($fileName)){
    echo "File not exists\n";
    exit;
}

$step1 = "nice -n 19 ffmpeg -i $fileName -af loudnorm=I=$target_I:dual_mono=true:TP=$target_TP:LRA=$target_LRA:print_format=json -f null - 2>&1 | awk '{print}' ORS=''";

echo " ---> step1";
$output = shell_exec($step1);

$output = preg_replace('/\s+/', '', $output);
if (!preg_match('/{.+}/',$output, $json)){
    echo " json result not found.\n";
    exit;
}
$measured = json_decode($json[0]);

$step2 = "nice -n 19 ffmpeg -i $fileName -af loudnorm=I=-16:TP=-1.5:LRA=11:measured_I=".$measured->input_i.":measured_TP=".$measured->input_tp.":measured_LRA=".$measured->input_lra.":measured_thresh=".$measured->output_thresh.":offset=".$measured->target_offset.":linear=true:print_format=summary $tempFileName 2>&1";

echo " ---> step2";
shell_exec($step2);

echo " ---> overwrite";
shell_exec("mv $tempFileName $fileName");

echo " ---> normalized.\n";